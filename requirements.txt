IPython==8.9.0
numpy==1.24.2
matplotlib==3.6.3
obspy==1.4.0
ipywidgets==8.0.4
h5py==3.8.0
scipy==1.10.0